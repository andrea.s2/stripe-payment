const stripe = Stripe("pk_test_qiGYa3BvuMYR5UYtljLOybA2");

// The items the customer wants to buy
const items = [{ id: "prod_Ka6VCOT0XZvKmd" }];

let elements;

initialize();

document
  .querySelector('#charge-form')
  .addEventListener("charge", handlerCharge)

async function handlerCharge(e) {
  console.log(e);
}

async function initialize (){
  elements = stripe.elements({currencies})
  const paymentElement = elements.create("payment");
  paymentElement.mount("#payment-element");
}  
  